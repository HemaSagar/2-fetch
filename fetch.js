const fetch = require('node-fetch');
/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

// Using promises and the `fetch` library, do the following. 
// If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.
// Usage of the path libary is recommended



const usersURL = 'https://jsonplaceholder.typicode.com/users';
const todosURL = 'https://jsonplaceholder.typicode.com/todos';

const userIdURL = 'https://jsonplaceholder.typicode.com/users?id=2302913';
const userTodoURL = 'https://jsonplaceholder.typicode.com/todos?userId=2321392';

function fetchURL(usersURL) {

    return fetch(usersURL)
        .then((response) => {
            if (!(response.ok)) {
                throw new Error(`HTTP error ${response.status}`);
            }
            else {
                return response.json();
            }
        })
}


// 1. Fetch all the users

const fetchUsers = (usersURL) => {
    fetchURL(usersURL)
    .then((allUsersData) => {
        console.log('1- all users data :\n ',allUsersData);
    })
    .catch((err) => {
        console.error(`1- ERROR occurred while fetching USERS details\n ${err}`);
    })
}



// 2. Fetch all the todos

const fetchTodos = (todosURL) => {

    fetchURL(todosURL)
    .then((allTodosData) => {
        console.log('2- All todos Data: \n',allTodosData);
    })
    .catch((err) => {
        console.error(`2- ERROR occurred while fetching TODOS details\n ${err}`);
    })
}



// 3. Use the promise chain and fetch the users first and then the todos.


const fetchUsersThenTodos = (usersURL, todosURL) => {
    fetchURL(usersURL)
        .then((allUsersData) => {
            console.log('3.1- all users data :\n ',allUsersData);

            //fetching todos after fetching users.
            return fetchURL(todosURL);
        })
        .then((allTodosData) => {
            console.log("\nfetching todos after fetching users\n");
            console.log('3.2- All todos Data: \n',allTodosData);
        })
        .catch((err) => {
            console.error(`3-ERROR occurred while fetching details\n ${err}`);
        })
}


// 4. Use the promise chain and fetch the users first and then all the details for each user.

const allUsersThenEachUser = (usersURL) => {

    fetchURL(usersURL)
        .then((allUsersData) => {
            const userPromises = allUsersData.map((user) => {
                let userURL = `https://jsonplaceholder.typicode.com/users?id=${user.id}`;
                return fetchURL(userURL);
            })

            return Promise.all(userPromises);
        })
        .then((usersDataFetchedById) => {
            console.log("4- Users data feteched by their Ids- \n",usersDataFetchedById);
        })
        .catch((err) => {
            console.error(`4-ERROR occurred while fetching details\n ${err}`);
        })
        

}



// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo


const userDetailsForFirstTodo = (todosURL) => {
    fetchURL(todosURL)
    .then((allTodosData) => {
        return allTodosData.filter((todo) => {
            return todo.id === 1;
        })[0].userId;
    })
    .then((firstTodoUserId) => {
        const userURL = `https://jsonplaceholder.typicode.com/users?id=${firstTodoUserId}`;
        return fetchURL(userURL);
    })
    .then((firstTodoUserData) => {
        console.log('5- details of user associated with first todo:\n ',firstTodoUserData);
    })
    .catch((err) => {
        console.error(`5-ERROR occurred while fetching details\n ${err}`);
    })
}




//1
fetchUsers(usersURL);
//2
fetchTodos(todosURL);
//3
fetchUsersThenTodos(usersURL, todosURL);
//4
allUsersThenEachUser(usersURL);
//5
userDetailsForFirstTodo(todosURL);